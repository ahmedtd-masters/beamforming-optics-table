import argparse
import csv
from itertools import islice
import numpy as np
from scipy.constants import pi, c

# Given a csv file of intrinsic offsets and a csv file of element
# positions, compute the real phase offset to target.
#
# The beam is assumed to be fired on the x-axis.

def parse_args():
    parser = argparse.ArgumentParser('Blah')
    parser.add_argument('--intrinsic-offsets', required=True)
    parser.add_argument('--element-positions', required=True)
    parser.add_argument('--frequency', type=float, required=True)
    parser.add_argument('--spacing', type=float, required=True)
    return parser.parse_args()

def angle_in_segment(angle, lo, hi):
    angle = angle % (2 * pi)
    lo = lo % (2 * pi)
    hi = hi % (2 * pi)

    if lo <= hi:
        return lo <= angle and angle < hi
    else:
        return (lo <= angle) or (angle < hi)

def angular_distance(a, b):
    dif = (a - b) % (2 * pi)
    if dif < 0:
        dif = dif + (2 * pi)
    elif dif > pi:
        dif = dif - (2 * pi)
    return -dif

def main():
    args = parse_args()

    wavelength = c / args.frequency
    wavenumber = (2 * pi) / wavelength

    intrinsic_offsets = {}
    with open(args.intrinsic_offsets, 'r') as csvfile:
        for row in islice(csv.reader(csvfile), 1, None):
            intrinsic_offsets[row[0]] = np.angle(float(row[1]) + 1j * float(row[2]))

    positional_offsets = {}
    with open(args.element_positions, 'r') as csvfile:
        for row in islice(csv.reader(csvfile), 1, None):
            positional_offsets[row[0]] = float(row[1]) * args.spacing * wavenumber

    real_offsets = {elt: (offset + intrinsic_offsets[elt]) for elt, offset in positional_offsets.items()}

    ten_percent = 2 * pi * 0.10
    five_percent = 2 * pi * 0.05

    max_count = -1
    max_elt = -1
    for elt, lo in real_offsets.items():
        captured = [v for v in real_offsets.values() if angle_in_segment(v, lo, lo + ten_percent)]
        if len(captured) > max_count:
            max_count = len(captured)
            max_elt = elt

    max_offset = real_offsets[max_elt]

    # Compute the angular distance from the center of the chosen
    # interval, for each element.
    distances_from_center = {elt: abs(angular_distance(ang, max_offset + five_percent)) for elt, ang in real_offsets.items()}
    sorted_real_offsets = list(real_offsets.items())
    sorted_real_offsets.sort(key=lambda x: distances_from_center[x[0]])

    print('element, offset, distance')
    for label, offset in sorted_real_offsets:
        print('{}, {}, {}'.format(label, offset, distances_from_center[label]))

    import matplotlib.pyplot as plt
    real_offsets_angles = np.array(list(real_offsets.values()))
    plt.scatter(np.cos(real_offsets_angles),
                np.sin(real_offsets_angles))
    for label, angle in real_offsets.items():
        plt.annotate(label, (np.cos(angle), np.sin(angle)))
    plt.annotate('up', (np.cos(max_offset + ten_percent)+0.05, np.sin(max_offset + ten_percent) + 0.05))
    plt.annotate('lw', (np.cos(max_offset) + 0.05, np.sin(max_offset) + 0.05))
    plt.xlim(-1.5, 1.5)
    plt.ylim(-1.5, 1.5)
    plt.show()


if __name__ == '__main__':
    main()
