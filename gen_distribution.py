import numpy as np
import random

# x is along the beamforming axis, y is across the table.

VALID_XCOORDS = list(range(60))
VALID_YCOORDS = list(range(48))

# The missing ones are intentional: 10, 29
PRESENT_ELEMENTS = ['1', '2', '3', '4', '5', '6', '7', '8', '9',
                    '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22',
                    '23', '24', '25', '26', '27', '28', '30', '31']


def gen_coord():
    x = random.choice(VALID_XCOORDS)
    y = random.choice(VALID_YCOORDS)

    return (x, y)

def distance(a, b):
    return np.sqrt((a[0] - b[0])**2 + (a[1] - b[1])**2)

def collides(coord, all_coords):
    for other in all_coords:
        if distance(coord, other) < 5:
            return True
    return False

def main():
    random.seed(12345)

    all_coords = []

    # We have 31 elements on the table.
    for _ in PRESENT_ELEMENTS:
        while True:
            coord = gen_coord()
            if not collides(coord, all_coords):
                all_coords.append(coord)
                break

    # Sort coordinates by y-coordinate, to simplify cabling.
    all_coords.sort(key=lambda x: x[1])

    print('element, x, y')
    for label, coord in zip(PRESENT_ELEMENTS, all_coords):
        print('{}, {}, {}'.format(label, coord[0], coord[1]))

if __name__ == '__main__':
    main()
