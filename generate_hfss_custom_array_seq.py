'''Given a csv of intrinsic offsets and a csv of element positions,
produce series of HFSS custom array files.  The first file contains
all elements transmitting at full power.  Each subsequent file
deactivates the element that is furthest from the most common phase.

'''

import argparse
import csv
from itertools import islice
import numpy as np
from scipy.constants import pi, c

def parse_args():
    parser = argparse.ArgumentParser('Blah')
    parser.add_argument('--intrinsic-offsets', required=True)
    parser.add_argument('--element-positions', required=True)
    parser.add_argument('--frequency', type=float, required=True)
    parser.add_argument('--spacing', type=float, required=True)
    return parser.parse_args()

def angle_in_segment(angle, lo, hi):
    angle = angle % (2 * pi)
    lo = lo % (2 * pi)
    hi = hi % (2 * pi)

    if lo <= hi:
        return lo <= angle and angle < hi
    else:
        return (lo <= angle) or (angle < hi)

def angular_distance(a, b):
    dif = (a - b) % (2 * pi)
    if dif < 0:
        dif = dif + (2 * pi)
    elif dif > pi:
        dif = dif - (2 * pi)
    return -dif

def main():
    args = parse_args()

    wavelength = c / args.frequency
    wavenumber = (2 * pi) / wavelength
    
    intrinsic_s21 = {}
    with open(args.intrinsic_offsets, 'r') as csvfile:
        for row in islice(csv.reader(csvfile), 1, None):
            intrinsic_s21[row[0]] = float(row[1]) + 1j * float(row[2])

    positions = {}
    with open(args.element_positions, 'r') as csvfile:
        for row in islice(csv.reader(csvfile), 1, None):
            positions[row[0]] = np.asarray(row[1:], dtype=float) * args.spacing

    positional_offsets = {elt: position[0] * args.spacing * wavenumber for elt, position in positions.items()}
            
    # Compute the offset that maximizes constructive interference among our population.

    real_offsets = {elt: (offset + np.angle(intrinsic_s21[elt])) for elt, offset in positional_offsets.items()}

    ten_percent = 2 * pi * 0.10
    five_percent = 2 * pi * 0.05

    max_count = -1
    max_elt = -1
    for elt, lo in real_offsets.items():
        captured = [v for v in real_offsets.values() if angle_in_segment(v, lo, lo + ten_percent)]
        if len(captured) > max_count:
            max_count = len(captured)
            max_elt = elt

    max_offset = real_offsets[max_elt]

    # Compute the angular distance from the center of the chosen interval, for each element.
    distances_from_center = {elt: abs(angular_distance(ang, max_offset + five_percent)) for elt, ang in real_offsets.items()}
    sorted_real_offsets = list(real_offsets.items())
    sorted_real_offsets.sort(key=lambda x: distances_from_center[x[0]])

    # Write out the file.
    # HFSS model units are mm, so we need to multiply the position by 1000.
    for i in range(len(sorted_real_offsets)):
        with open('{:03d}.customarray.txt'.format(i), 'w') as ofile:
            ofile.write('{}\n'.format(len(sorted_real_offsets)))
            for j in range(len(sorted_real_offsets)):
                elt, _ = sorted_real_offsets[j]
                data = {'x': positions[elt][0] * 1000,
                        'y': positions[elt][1] * 1000,
                        'z': 0,
                        'a': np.abs(intrinsic_s21[elt]),
                        'p': np.angle(intrinsic_s21[elt])}
                if i < j:
                    data['a'] = 0
                ofile.write('{x} {y} {z} {a} {p}\n'.format(**data))
                

    # intrinsic_phase = {elt: np.angle(s21) for elt, s21 in intrinsic_s21.items()}
    # intrinsic_mag = {elt: np.abs(s21) for elt, s21 in intrinsic_s21.items()}

    # rows = [ for elt in positions]

    # print(len(rows))
    # for row in rows:
    #     print('{x} {y} {z} {a} {p}'.format(**row))

main()
