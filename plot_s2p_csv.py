'''Load an s2p file saved by an Agilent Network Analyzer'''

import argparse
import numpy as np

from scipy.constants import pi
from skrf.io.touchstone import Touchstone

def get_phase_offset_from_touchstone_file(touchstone_file, sweep_index):
    # f is the frequency vector, s is a numpy vector of complex s-matrices.
    cable_file = Touchstone(touchstone_file)
    f,s = cable_file.get_sparameter_arrays()
    return s[sweep_index, 1, 0]

def parse_args():
    parser = argparse.ArgumentParser(description='Output center phase offset for a set of touchstone files.')

    parser.add_argument('--sweep-index',
                        default=300,
                        type=int)

    parser.add_argument('filenames',
                        nargs='+')

    return parser.parse_args()

def main():
    args = parse_args()

    args.filenames.sort()

    per_file_s21 = np.empty((len(args.filenames),), dtype=np.complex)

    for (i, filename) in enumerate(args.filenames):
        with open(filename, 'r') as touchstone_file:
            per_file_s21[i] = get_phase_offset_from_touchstone_file(touchstone_file,
                                                                    args.sweep_index)
    s21_db = 20 * np.log10(np.absolute(per_file_s21))
    s21_ph = np.angle(per_file_s21)

    print('index, num_active_elements, s21_db')
    for i in range(len(s21_db)):
        print('{i}, {numactive}, {db}'
              .format(i=i,
                      numactive=(len(s21_db)-i),
                      db=s21_db[i]))

if __name__ == '__main__':
    main()
