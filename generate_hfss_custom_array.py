'''Given a csv of intrinsic offsets, a csv of element positions, produce an
HFSS custom array file that describes the array.

'''

import argparse
import csv
from itertools import islice
import numpy as np
from scipy.constants import pi, c

def parse_args():
    parser = argparse.ArgumentParser('Blah')
    parser.add_argument('--intrinsic-offsets', required=True)
    parser.add_argument('--element-positions', required=True)
    parser.add_argument('--spacing', type=float, required=True)
    return parser.parse_args()

def main():
    args = parse_args()

    intrinsic_s21 = {}
    with open(args.intrinsic_offsets, 'r') as csvfile:
        for row in islice(csv.reader(csvfile), 1, None):
            intrinsic_s21[row[0]] = float(row[1]) + 1j * float(row[2])

    positions = {}
    with open(args.element_positions, 'r') as csvfile:
        for row in islice(csv.reader(csvfile), 1, None):
            positions[row[0]] = np.asarray(row[1:], dtype=float) * 1000 * args.spacing

    intrinsic_phase = {elt: np.angle(s21) for elt, s21 in intrinsic_s21.items()}
    intrinsic_mag = {elt: np.abs(s21) for elt, s21 in intrinsic_s21.items()}

    rows = [{'x': positions[elt][0],
             'y': positions[elt][1],
             'z': 0,
             'a': intrinsic_phase[elt],
             'p': intrinsic_mag[elt]} for elt in positions]

    print(len(rows))
    for row in rows:
        print('{x} {y} {z} {a} {p}'.format(**row))

main()
