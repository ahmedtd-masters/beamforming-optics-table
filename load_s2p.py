'''Load an s2p file saved by an Agilent Network Analyzer'''

import argparse
from   os.path import basename, splitext

import matplotlib.pyplot as plt
import numpy as np

from scipy.constants import pi
from skrf.io.touchstone import Touchstone

def get_phase_offset_from_touchstone_file(touchstone_file, sweep_index):
    # f is the frequency vector, s is a numpy vector of complex s-matrices.
    cable_file = Touchstone(touchstone_file)
    f,s = cable_file.get_sparameter_arrays()
    return s[sweep_index, 1, 0]

def parse_args():
    parser = argparse.ArgumentParser(
        description='Output center phase offset for a set of touchstone files.'
    )

    parser.add_argument(
        '--sweep-index',
        default=500,
        type=int
    )

    parser.add_argument(
        'filenames',
        nargs='+'
    )

    return parser.parse_args()

def main():
    args = parse_args()

    args.filenames.sort()

    per_file_s21 = {}

    for filename in args.filenames:
        with open(filename, 'r') as touchstone_file:
            name = splitext(basename(filename))[0]
            per_file_s21[name] = get_phase_offset_from_touchstone_file(touchstone_file,
                                                                       args.sweep_index)


    print('element, a, b')
    for name, val in per_file_s21.items():
        print('{}, {}, {}'.format(name, np.real(val), np.imag(val)))


    # plt.scatter(np.real(per_file_s21), np.imag(per_file_s21))
    # plt.xlim(-1, 1)
    # plt.ylim(-1, 1)
    # plt.show()

if __name__ == '__main__':
    main()
